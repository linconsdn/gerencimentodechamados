--create schema gerenciamento_de_horas;

set search_path to gerenciamento_de_horas;

create sequence gerenciamento_de_horas.sq_usuario start with 1 increment by 1;
create table gerenciamento_de_horas.usuario(
 id_usuario int primary key default NextVal('gerenciamento_de_horas.sq_usuario'),
 nome varchar(100),
 email varchar(150)
);

create sequence gerenciamento_de_horas.sq_tipo_atendimento start with 1 increment by 1;
create table gerenciamento_de_horas.tipo_atendimento(
 id_tipo_atendimento int primary key default NextVal('gerenciamento_de_horas.sq_tipo_atendimento'),
 tipo varchar(10),
 descricao varchar(50)
);

create sequence gerenciamento_de_horas.sq_plataforma start with 1 increment by 1;
create table gerenciamento_de_horas.plataforma(
id_plataforma int primary key default nextval('gerenciamento_de_horas.sq_plataforma'),
nome varchar(4)
);

create sequence gerenciamento_de_horas.sq_status start with 1 increment by 1;
create table gerenciamento_de_horas.status(
id_status int primary key default nextval('gerenciamento_de_horas.sq_status'),
nome varchar(50) not null,
descricao varchar(100),
tipoatendimento int,
plataforma int,

foreign key (tipoatendimento) references gerenciamento_de_horas.tipo_atendimento(id_tipo_atendimento),
foreign key (plataforma) references gerenciamento_de_horas.plataforma(id_plataforma)
);

create table gerenciamento_de_horas.fila_atendimento (
id_atendimento int primary key,
datainicio date,
datafim date,
horastrabalhadas time,
usuario int,
tipoatendimento int,
jira int,
sdm int,

foreign key (usuario) references gerenciamento_de_horas.usuario(id_usuario),
foreign key (tipoatendimento) references gerenciamento_de_horas.tipo_atendimento(id_tipo_atendimento),
foreign key (jira) references gerenciamento_de_horas.status(id_status),
foreign key (sdm) references gerenciamento_de_horas.status(id_status)
);


