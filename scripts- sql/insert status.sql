set search_path to gerenciamento_de_horas;

--select * from tipo_atendimento ta;
--select * from plataforma p;
-- status Incidente(2) jira(1)
insert into status(nome, descricao, tipoatendimento, plataforma)
values('Aberto', 'Pronto para ser atendido', 2, 1);

insert into status(nome, descricao, tipoatendimento, plataforma)
values('Em atendimento', 'Item encontra-se em atendimento', 2, 1);

insert into status(nome, descricao, tipoatendimento, plataforma)
values('Aguardando', 'Item aguardando informa��es', 2, 1);

insert into status(nome, descricao, tipoatendimento, plataforma)
values('Atualitab', 'Item aguardando execu��o de scripts .sql', 2, 1);

insert into status(nome, descricao, tipoatendimento, plataforma)
values('Encerrado', 'Solu��o aplicada', 2, 1);

select * from status s where s.tipoatendimento = 1 and s.plataforma = 1;

select * from status s where s.plataforma = 2;

-- status Problema(1) jira(1)
insert into status(nome, descricao, tipoatendimento, plataforma)
values('Pendente', 'Pronto para ser atendido', 1, 1);

insert into status(nome, descricao, tipoatendimento, plataforma)
values('Impedimento', 'Item aguardando resolu��o de algum impedimento com a seguradora', 1, 1);

insert into status(nome, descricao, tipoatendimento, plataforma)
values('Priorizado', 'Pronto para ser atendido, com urgencia', 1, 1);

insert into status(nome, descricao, tipoatendimento, plataforma)
values('Em atendimento', 'Item em analise', 1, 1);

select * from status s where s.tipoatendimento = 1 and s.plataforma = 1;

-- status Incidente(2) SDM(2)
insert into status(nome, descricao, tipoatendimento, plataforma)
values('Aberto', 'Pronto para ser atendido', 2, 2);

insert into status(nome, descricao, tipoatendimento, plataforma)
values('Em atendimento', 'Item encontra-se em atendimento', 2, 2);

insert into status(nome, descricao, tipoatendimento, plataforma)
values('Aguardando cliente', 'Item aguardando informa��es do cliente', 2, 2);

insert into status(nome, descricao, tipoatendimento, plataforma)
values('Resolvido', 'Item foi solucionada', 2, 2);

--select * from status s where s.tipoatendimento = 2 and s.plataforma = 2;

-- status Problema(1) SDM(1)
insert into status(nome, descricao, tipoatendimento, plataforma)
values('Aberto', 'Pronto para ser atendido', 1, 2);

insert into status(nome, descricao, tipoatendimento, plataforma)
values('Em atendimento', 'Item encontra-se em atendimento', 1, 2);

insert into status(nome, descricao, tipoatendimento, plataforma)
values('Em planejamento', 'Item em investiga��o', 1, 2);

insert into status(nome, descricao, tipoatendimento, plataforma)
values('Resolvido', 'Corre��o na causa  raiz foi aplicada', 1, 2);

--update status set plataforma = 2 where id_status >= 14;

select * from status s where s.tipoatendimento = 1 and s.plataforma = 2;