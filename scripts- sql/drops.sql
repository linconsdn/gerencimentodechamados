set search_path to gerenciamento_de_horas;
--drop table
drop table gerenciamento_de_horas.fila_atendimento;
drop table gerenciamento_de_horas.status;
drop table gerenciamento_de_horas.tipo_atendimento;
drop table gerenciamento_de_horas.plataforma;
drop table gerenciamento_de_horas.usuario;

--drop sequence
drop sequence gerenciamento_de_horas.sq_status;
drop sequence gerenciamento_de_horas.sq_usuario;
drop sequence gerenciamento_de_horas.sq_tipo_atendimento;
drop sequence gerenciamento_de_horas.sq_plataforma;