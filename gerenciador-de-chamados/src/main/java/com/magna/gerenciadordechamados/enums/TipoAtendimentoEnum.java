package com.magna.gerenciadordechamados.enums;

public enum TipoAtendimentoEnum {
	PB(1),
	INC(2),
	SL(3); 
	
	private Integer value;
	
	TipoAtendimentoEnum(Integer value) {
        this.value = value;
    }

    public Integer getValue() {
        return value;
    }

	
	
}
