package com.magna.gerenciadordechamados.enums;

public enum PlataformaEnum {
	JIRA(1),
	SDM(2);
	
	private Integer value;
	
	PlataformaEnum(Integer value){
		this.value = value;
	}
	
	public Integer getValue() {
		return this.value;
	}
}
