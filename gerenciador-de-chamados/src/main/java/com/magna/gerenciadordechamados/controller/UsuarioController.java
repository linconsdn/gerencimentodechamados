package com.magna.gerenciadordechamados.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.magna.gerenciadordechamados.DTO.UsuarioDTO;
import com.magna.gerenciadordechamados.model.Usuario;
import com.magna.gerenciadordechamados.service.UsuarioService;

@CrossOrigin
@RestController
@RequestMapping("/api")
public class UsuarioController {
	
	@Autowired
	UsuarioService usuarioService;
	
	@GetMapping(value = "/usuario")
	private List<UsuarioDTO> getAllUsuarios(){
		return usuarioService.getAllUsuarios();
	}
	
	@GetMapping("/usuario/{idUsuario}")
	private UsuarioDTO getUsuario(@PathVariable Integer idUsuario) {
		return UsuarioDTO.transformaEmDTO(usuarioService.getUsuario(idUsuario));
	}
	
	@PostMapping("/usuario")
	private UsuarioDTO save(@RequestBody UsuarioDTO usuario) {
		usuarioService.saveOrUpdate(UsuarioDTO.transformaEmObjeto(usuario));
		return usuario;
	}
	
	@DeleteMapping("/usuario")
	private void delete(@PathVariable Integer idUsuario) {
		usuarioService.delete(idUsuario);
	}
	
	@PutMapping("/usuario")
	private Usuario update(@RequestBody Usuario usuario) {
		usuarioService.saveOrUpdate(usuario);
		return usuario;
	}
}
