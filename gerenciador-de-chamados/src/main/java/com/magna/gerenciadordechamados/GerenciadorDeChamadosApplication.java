package com.magna.gerenciadordechamados;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;
//import org.springframework.boot.builder.SpringApplicationBuilder;
//import org.springframework.context.annotation.ComponentScan;

import com.magna.gerenciadordechamados.controller.UsuarioController;

@SpringBootApplication
@EntityScan(basePackages = {"com.magna.gerenciadordechamados.model"})
@ConfigurationPropertiesScan(basePackageClasses = UsuarioController.class )
public class GerenciadorDeChamadosApplication {

	public static void main(String[] args) {
		SpringApplication.run(GerenciadorDeChamadosApplication.class, args);
	}
//	
//    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
//        return application.sources(GerenciadorDeChamadosApplication.class);
//    }

}
