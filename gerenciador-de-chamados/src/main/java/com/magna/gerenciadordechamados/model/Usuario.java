package com.magna.gerenciadordechamados.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.magna.gerenciadordechamados.constants.Constantes;

import lombok.Getter;
import lombok.Setter;

@Getter
@Entity
@Table(name = "usuario", schema = Constantes.SCHEMA)
public class Usuario {
	
	@Id
	@SequenceGenerator(name = Constantes.SEQUENCE_USUARIO, sequenceName = Constantes.SEQUENCE_USUARIO, allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = Constantes.SEQUENCE_USUARIO)
	@Column(name = "id_usuario")
	private Integer idUsuario;
	
	@Column(name = "nome")
	@Setter private String nome;
	
	@Column(name = "email")
	@Setter private String email;
	
	@OneToMany(mappedBy = "usuario", fetch = FetchType.LAZY)
	@Setter private List<FilaAtendimento> filaAtendimento;

	public Usuario(String nome, String email) {
		this.nome = nome;
		this.email = email;
	}

	public Usuario() {}
	


}
