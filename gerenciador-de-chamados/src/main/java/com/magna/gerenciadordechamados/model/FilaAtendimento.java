package com.magna.gerenciadordechamados.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.magna.gerenciadordechamados.constants.Constantes;

import lombok.Data;

@Data
@Entity
@Table(name = "fila_atendimento", schema = Constantes.SCHEMA)
public class FilaAtendimento {
	
	@Id
	@Column(name = "id_atendimento")
	private Integer codigoAtendimento;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "datainicio")
	private Date dataInicio;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "datafim")
	private Date dataFim;
	
	@Temporal(TemporalType.TIME)
	@Column(name = "horastrabalhadas")
	private Date horasTrabalhadas;
	
	@JoinColumn(name = "usuario", referencedColumnName = "id_usuario")
	@ManyToOne(fetch = FetchType.LAZY)
	private Usuario usuario;
	
	@JoinColumn(name = "tipoatendimento", referencedColumnName = "id_tipo_atendimento")
	@ManyToOne(fetch = FetchType.EAGER)
	private TipoAtendimento tipoAtendimento;
	
	@JoinColumn(name = "jira", referencedColumnName = "id_status")
	@ManyToOne(fetch = FetchType.LAZY)
	private Status jira;
	
	@JoinColumn(name = "sdm", referencedColumnName = "id_status")
	@ManyToOne(fetch = FetchType.LAZY)
	private Status sdm;

}
