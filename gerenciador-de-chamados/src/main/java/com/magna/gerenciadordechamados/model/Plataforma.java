package com.magna.gerenciadordechamados.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.magna.gerenciadordechamados.constants.Constantes;

import lombok.Getter;
import lombok.Setter;

@Getter
@Entity
@Table(name = "plataforma", schema = Constantes.SCHEMA)
public class Plataforma {
	
	@Id
	@SequenceGenerator(name = Constantes.SEQUENCE_PLATAFORMA, sequenceName = Constantes.SEQUENCE_PLATAFORMA, allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = Constantes.SEQUENCE_PLATAFORMA)
	@Column(name = "id_plataforma")
	private Integer idPlataforma;
	
	@Column(name = "nome")
	@Setter private String nome;
	
	@OneToMany(mappedBy = "tipoAtendimento")
	@Setter private List<Status> status;
}
