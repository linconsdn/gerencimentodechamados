package com.magna.gerenciadordechamados.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.magna.gerenciadordechamados.constants.Constantes;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Getter
@Entity
@Table(name = "status", schema = Constantes.SCHEMA)
public class Status {
	
	@Id
	@SequenceGenerator(name = Constantes.SEQUENCE_STAUTUS, sequenceName = Constantes.SEQUENCE_STAUTUS, allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = Constantes.SEQUENCE_STAUTUS)
	@Column(name = "id_status")
	 private Integer idStatus;
	
	@Column(name = "nome")
	private String nome;
	
	@Column(name = "descricao")
	@Setter private String descricao;
	
	@JoinColumn(name = "tipoatendimento", referencedColumnName = "id_tipo_atendimento")
	@ManyToOne(fetch = FetchType.LAZY)
	@Setter private TipoAtendimento tipoAtendimento;
	
	@JoinColumn(name = "plataforma", referencedColumnName = "id_plataforma")
	@ManyToOne(fetch = FetchType.LAZY)
	@Setter private Plataforma plataforma;
	
	@OneToMany(mappedBy = "jira")
	@Setter private List<FilaAtendimento> filaAtendimentoJira;
	
	@OneToMany(mappedBy = "sdm")
	@Setter private List<FilaAtendimento> filaAtendimentoSdm;

}
