package com.magna.gerenciadordechamados.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.magna.gerenciadordechamados.constants.Constantes;

import lombok.Getter;
import lombok.Setter;

@Getter
@Entity
@Table(name = "tipo_atendimento", schema = Constantes.SCHEMA)
public class TipoAtendimento {
	
	@Id
	@SequenceGenerator(name = Constantes.SEQUENCE_TIPO_ATENDIMENTO, sequenceName = Constantes.SEQUENCE_TIPO_ATENDIMENTO, allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = Constantes.SEQUENCE_TIPO_ATENDIMENTO)
	@Column(name="id_tipo_atendimento")
	private Integer idTipoAtendimento;
	
	@Column(name = "tipo")
	@Setter private String tipo;
	
	@Column(name = "descricao")
	@Setter private String descricao;
	
	@OneToMany(mappedBy = "tipoAtendimento")
	@Setter private List<Status> status;
	
	@OneToMany(mappedBy = "tipoAtendimento")
	@Setter private List<FilaAtendimento> filaAtendimento;
	
}
