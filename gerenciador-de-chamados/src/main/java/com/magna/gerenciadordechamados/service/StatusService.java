package com.magna.gerenciadordechamados.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.magna.gerenciadordechamados.enums.PlataformaEnum;
import com.magna.gerenciadordechamados.enums.TipoAtendimentoEnum;
import com.magna.gerenciadordechamados.model.Status;
import com.magna.gerenciadordechamados.repository.StatusRepository;

@Service
public class StatusService {
	
	@Autowired
	StatusRepository statusRepository;
	
	@Autowired
	TipoAtendimentoService tipoAtendimentoService;
	
	@Autowired
	PlataformaService plataformaService;
	
	public List<Status> findAll(){
		List<Status> status = new ArrayList<Status>();
		statusRepository.findAll().forEach(status::add);
		return status;
	}
	
	public List<Status> findByTipoAtendimento(TipoAtendimentoEnum tipoAtendimentoEnum) {
		List<Status> status = new ArrayList<Status>();
		statusRepository.findByTipoAtendimento(tipoAtendimentoService
				.findById(tipoAtendimentoEnum.getValue()))
		.forEach(statu -> status.add(statu));
		return status;
	}
	
	public List<Status> findByPlataforma(PlataformaEnum plataformaEnum){
		List<Status> status = new ArrayList<Status>();
		statusRepository.findByPlataforma(plataformaService
				.findById(plataformaEnum.getValue()))
		.forEach(statu -> status.add(statu));;			
		return status;
	}

	public List<Status> findByPlataformaAndTipoAtendimento(PlataformaEnum plataformaEnum, TipoAtendimentoEnum tipoAtendimentoEnum){
		List<Status> status = new ArrayList<Status>();
		statusRepository.findByPlataformaAndTipoAtendimento(plataformaService
				.findById(plataformaEnum.getValue()),
				tipoAtendimentoService.findById(tipoAtendimentoEnum.getValue()))
		.forEach(statu -> status.add(statu));
		return status;
	}
	
	public Status findById(Integer id) {
		return statusRepository.findById(id).get();
	}
}
