package com.magna.gerenciadordechamados.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.magna.gerenciadordechamados.model.FilaAtendimento;
import com.magna.gerenciadordechamados.model.Status;
import com.magna.gerenciadordechamados.model.TipoAtendimento;
import com.magna.gerenciadordechamados.model.Usuario;
import com.magna.gerenciadordechamados.repository.FilaAtendimentoRepository;

@Service
public class FilaAtendimentoService {
	
	@Autowired
	FilaAtendimentoRepository filaAtendimentoRepository;
	
	public FilaAtendimento findById(Integer id) {
		return filaAtendimentoRepository.findById(id).get();
	}
	
	public void save(FilaAtendimento filaAtendimento) {
		filaAtendimentoRepository.save(filaAtendimento);
	}
	
	public Iterable<FilaAtendimento> findByUsuario(Usuario usuario){
		return filaAtendimentoRepository.findByUsuario(usuario);	
	}
	
	public List<FilaAtendimento> findByTipoAtendimento(TipoAtendimento tipoAtendimento){
		return filaAtendimentoRepository.findByTipoAtendimento(tipoAtendimento);	 
	}
	
//	public List<FilaAtendimento> findByJiraAndTipoAtendimento(Status jira, TipoAtendimento tipoAtendimento){
//		return filaAtendimentoRepository.findByJiraAndTipoAtendimento(jira, tipoAtendimento);
//	}
	
	public List<FilaAtendimento> findBySdmAndTipoAtendimento(Status sdm, TipoAtendimento tipoAtendimento){
		return filaAtendimentoRepository.findByJiraAndTipoAtendimento(sdm, tipoAtendimento);	
	}
	
	public List<FilaAtendimento> findByUsuarioAndTipoAtendimento(Usuario usuario, TipoAtendimento tipoAtendimento){
		return filaAtendimentoRepository.findByUsuarioAndTipoAtendimento(usuario, tipoAtendimento);	
	}
	
	public List<FilaAtendimento> findByDataInicioAfterAndJiraAndTipoAtendimento(Date dataInicio ,Status jira, TipoAtendimento tipoAtendimento){
		return filaAtendimentoRepository.findByDataInicioAfterAndJiraAndTipoAtendimento(dataInicio, jira, tipoAtendimento);	
	}
	
	public List<FilaAtendimento> findByJiraAndTipoAtendimento(Status jira, TipoAtendimento tipoAtendimento){
		return filaAtendimentoRepository.findByJiraAndTipoAtendimento(jira, tipoAtendimento);	
	}
	
	public List<FilaAtendimento> findByDataInicioAfterAndDataFimBefore(Date dataInicio, Date dataFim){
		return filaAtendimentoRepository.findByDataInicioAfterAndDataFimBefore(dataInicio, dataFim);	
	}

}
