package com.magna.gerenciadordechamados.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.magna.gerenciadordechamados.model.TipoAtendimento;
import com.magna.gerenciadordechamados.repository.TipoAtendimentoRepository;

@Service
public class TipoAtendimentoService {
	
	@Autowired
	TipoAtendimentoRepository tipoAtendimentoRepository;
	
	public List<TipoAtendimento> findAll(){
		List<TipoAtendimento> tipoAtendimentos = new ArrayList<TipoAtendimento>();
		tipoAtendimentoRepository.findAll().forEach(tipoAtendimentos::add);
		return tipoAtendimentos;
	}
	
	public TipoAtendimento findById(Integer id) {
		return tipoAtendimentoRepository.findById(id).get();
	}
}
