package com.magna.gerenciadordechamados.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.magna.gerenciadordechamados.DTO.UsuarioDTO;
import com.magna.gerenciadordechamados.model.Usuario;
import com.magna.gerenciadordechamados.repository.UsuarioRepository;

@Service
public class UsuarioService {
	
	@Autowired(required = true)
	UsuarioRepository usuarioRepository;
	
	public List<UsuarioDTO> getAllUsuarios(){ 
		List<UsuarioDTO> usuarios = new ArrayList<UsuarioDTO>();
		usuarioRepository.findAll().forEach(usuario -> usuarios.add(UsuarioDTO.transformaEmDTO(usuario)));
		return usuarios;
	}
	
	public Usuario getUsuario(Integer idUsuario) {
		return usuarioRepository.findById(idUsuario).get();
	}
	
	public void saveOrUpdate(Usuario usuario) {
		usuarioRepository.save(usuario);
	}
	
	public void delete(Integer idUsuario) {
		usuarioRepository.deleteById(idUsuario);
	}
	
	public Long count() {
		return  usuarioRepository.count();
	}
	

}
