package com.magna.gerenciadordechamados.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.magna.gerenciadordechamados.model.Plataforma;
import com.magna.gerenciadordechamados.repository.PlataformaRepository;

@Service
public class PlataformaService {
	
	@Autowired
	PlataformaRepository plataformaRepository;
	
	public Plataforma findById(Integer id) {
		return plataformaRepository.findById(id).get();
	}
}
