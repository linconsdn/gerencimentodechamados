package com.magna.gerenciadordechamados.DTO;

import com.magna.gerenciadordechamados.model.Usuario;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Getter
public class UsuarioDTO {

	private Integer idUsuario;
	private String nome;
	private String email;
	
	public UsuarioDTO() {
		
	}
	
	public UsuarioDTO(String nome, String email) {
		this.nome = nome;
		this.email = email;
	}
	
	public static UsuarioDTO transformaEmDTO(Usuario usuario) {
	    return new UsuarioDTO(usuario.getIdUsuario(), usuario.getNome(), usuario.getEmail());
	}
	
	public static Usuario transformaEmObjeto(UsuarioDTO usuario) {
	    return new Usuario(usuario.getNome(), usuario.getEmail());
	}

}
