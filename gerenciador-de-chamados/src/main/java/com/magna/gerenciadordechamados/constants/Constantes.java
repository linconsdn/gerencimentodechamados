package com.magna.gerenciadordechamados.constants;

public class Constantes {
	public static final String SCHEMA = "gerenciamento_de_horas";
	public static final String SEQUENCE_TIPO_ATENDIMENTO = "sq_tipo_atendimento";
	public static final String SEQUENCE_USUARIO = "sq_usuario";
	public static final String SEQUENCE_STAUTUS = "sq_plataforma";
	public static final String SEQUENCE_PLATAFORMA = "sq_status";
}
