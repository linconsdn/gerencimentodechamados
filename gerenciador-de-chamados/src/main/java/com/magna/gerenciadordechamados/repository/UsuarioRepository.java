package com.magna.gerenciadordechamados.repository;

import org.springframework.data.repository.CrudRepository;

import com.magna.gerenciadordechamados.model.Usuario;

public interface UsuarioRepository extends CrudRepository<Usuario, Integer> {

}
