package com.magna.gerenciadordechamados.repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jdbc.repository.query.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.magna.gerenciadordechamados.model.FilaAtendimento;
import com.magna.gerenciadordechamados.model.Status;
import com.magna.gerenciadordechamados.model.TipoAtendimento;
import com.magna.gerenciadordechamados.model.Usuario;

public interface FilaAtendimentoRepository extends CrudRepository<FilaAtendimento, Integer>{

	Iterable<FilaAtendimento> findByUsuario (Usuario usuario);
	List<FilaAtendimento> findByTipoAtendimento(TipoAtendimento tipoAtendimento);
	List<FilaAtendimento> findByJiraAndTipoAtendimento(Status jira, TipoAtendimento tipoAtendimento);
	List<FilaAtendimento> findBySdmAndTipoAtendimento(Status sdm, TipoAtendimento tipoAtendimento);
	List<FilaAtendimento> findByUsuarioAndTipoAtendimento(Usuario usuario, TipoAtendimento tipoAtendimento);
	//@Query("Select f from fila_atendimento f where f.tipoatendimento = :tipoAtendimento and f.jira = :jira and f.datainicio > :dataInicio")
	List<FilaAtendimento> findByDataInicioAfterAndJiraAndTipoAtendimento(Date dataInicio, Status jira, TipoAtendimento tipoAtendimento);
	List<FilaAtendimento> findByDataInicioAfterAndDataFimBefore(Date dataInicio, Date dataFim);
	
}
