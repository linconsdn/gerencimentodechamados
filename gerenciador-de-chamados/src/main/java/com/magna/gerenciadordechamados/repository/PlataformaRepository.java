package com.magna.gerenciadordechamados.repository;

import org.springframework.data.repository.CrudRepository;

import com.magna.gerenciadordechamados.model.Plataforma;

public interface PlataformaRepository extends CrudRepository<Plataforma, Integer> {

}
