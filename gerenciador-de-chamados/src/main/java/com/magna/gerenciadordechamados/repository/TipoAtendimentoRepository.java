package com.magna.gerenciadordechamados.repository;

import org.springframework.data.repository.CrudRepository;

import com.magna.gerenciadordechamados.model.TipoAtendimento;

public interface TipoAtendimentoRepository extends CrudRepository<TipoAtendimento, Integer>  {

}
