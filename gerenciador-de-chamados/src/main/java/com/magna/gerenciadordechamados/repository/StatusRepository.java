package com.magna.gerenciadordechamados.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.magna.gerenciadordechamados.model.Plataforma;
import com.magna.gerenciadordechamados.model.Status;
import com.magna.gerenciadordechamados.model.TipoAtendimento;

public interface StatusRepository extends CrudRepository<Status, Integer> {
	List<Status> findByTipoAtendimento(TipoAtendimento tipoAtendimento);
	List<Status> findByPlataforma(Plataforma plataforma);
	List<Status> findByPlataformaAndTipoAtendimento(Plataforma plataforma, TipoAtendimento tipoAtendimento);
}
