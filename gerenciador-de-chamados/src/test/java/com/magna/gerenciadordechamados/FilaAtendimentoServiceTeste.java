package com.magna.gerenciadordechamados;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.security.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.tomcat.jni.Time;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.magna.gerenciadordechamados.enums.TipoAtendimentoEnum;
import com.magna.gerenciadordechamados.model.FilaAtendimento;
import com.magna.gerenciadordechamados.service.FilaAtendimentoService;
import com.magna.gerenciadordechamados.service.PlataformaService;
import com.magna.gerenciadordechamados.service.StatusService;
import com.magna.gerenciadordechamados.service.TipoAtendimentoService;
import com.magna.gerenciadordechamados.service.UsuarioService;

@SpringBootTest
public class FilaAtendimentoServiceTeste {
	
	@Autowired
	FilaAtendimentoService filaAtendimentoService;
	
	@Autowired
	TipoAtendimentoService tipoAtendimentoService;
	
	@Autowired
	UsuarioService usuarioService;
	
	@Autowired
	StatusService statusService;
	
	@Autowired
	PlataformaService plataformaService;
	
	@Test
	void save() {
		
		FilaAtendimento filaAtendimento = new FilaAtendimento();
		
		filaAtendimento.setCodigoAtendimento(100);
		filaAtendimento.setTipoAtendimento(tipoAtendimentoService.findById(TipoAtendimentoEnum.PB.getValue()));
		filaAtendimento.setUsuario(usuarioService.getUsuario(1));
		filaAtendimento.setDataInicio(gerarData("dd/MM/yyyy", "7/04/2021"));
		filaAtendimento.setJira(statusService.findById(9));
		filaAtendimento.setSdm(statusService.findById(11));
		filaAtendimento.setDataFim(gerarData("dd/MM/yyyy", "11/04/2021"));
		filaAtendimento.setHorasTrabalhadas(null);
		
		filaAtendimentoService.save(filaAtendimento);
		
	}

	private Date gerarData(String forma, String dat) {
		SimpleDateFormat formato = new SimpleDateFormat(forma); 
		Date data = null;
		try {
			data = formato.parse(dat);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return data;
	}
	

	
	@Test
	void findById() {
		FilaAtendimento filaAtendimento = filaAtendimentoService.findById(100);
		
		System.out.println("codigo:" + filaAtendimento.getCodigoAtendimento());
		System.out.println("dataInicio:" + filaAtendimento.getDataInicio());
		
		if(filaAtendimento.getTipoAtendimento() != null && filaAtendimento.getTipoAtendimento().getTipo() != null) {
			System.out.println("TipoAtendimento:"+ filaAtendimento.getTipoAtendimento().getTipo());
		}
	}
	
	//@Test
	void findAll() {
		filaAtendimentoService.findByTipoAtendimento(tipoAtendimentoService
				.findById(TipoAtendimentoEnum.INC.getValue()))
		.forEach(fila -> System.out.println(fila.getCodigoAtendimento()));
	}
	//@Test
	void findByDataInicioAndJiraAndTipoAtendimento() {
		List<FilaAtendimento> filaAtendimento = filaAtendimentoService.findByDataInicioAfterAndJiraAndTipoAtendimento
		(gerarData("yyyy-MM-dd", "2021-03-29"),
				statusService.findById(5),
				tipoAtendimentoService.findById(TipoAtendimentoEnum.INC.getValue()));
		
		assertNotNull(filaAtendimento);
		System.out.println(filaAtendimento.size());
		filaAtendimento.forEach(fila -> System.out.println("codigo " + fila.getCodigoAtendimento()));
	}
	//@Test
	void findByDataInicioAftetAndDatafimBefore() {
		filaAtendimentoService.findByDataInicioAfterAndDataFimBefore(
				gerarData("yyyy-MM-dd", "2021-04-1"), 
				gerarData("yyyy-MM-dd", "2021-04-07"))
		.forEach(fila -> System.out.println(fila.getHorasTrabalhadas()));;
	}

}
