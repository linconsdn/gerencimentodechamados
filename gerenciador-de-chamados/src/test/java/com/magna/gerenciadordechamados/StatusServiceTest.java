package com.magna.gerenciadordechamados;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.magna.gerenciadordechamados.enums.PlataformaEnum;
import com.magna.gerenciadordechamados.enums.TipoAtendimentoEnum;
import com.magna.gerenciadordechamados.model.Status;
import com.magna.gerenciadordechamados.service.StatusService;

@SpringBootTest
public class StatusServiceTest {
	
	@Autowired
	StatusService statusService;
	
	@Test
	void findAll() {	
		List<Status> s = statusService.findAll();
		
		assertNotNull(s);
		validaQuantidade(s, 17);
	}

	
	@Test
	void findByTipoAtendimento() {
		List<Status> statusPb = statusService.findByTipoAtendimento(TipoAtendimentoEnum.PB);
		assertNotNull(statusPb);
		
		
		
		List<Status> statusInc = statusService.findByTipoAtendimento(TipoAtendimentoEnum.INC);
		assertNotNull(statusInc);
		
		validaQuantidade(statusService.findAll(), statusInc.size() + statusPb.size());
	}
	
	@Test
	void findByPlataforma() {
		List<Status> statusJira = statusService.findByPlataforma(PlataformaEnum.JIRA);
		assertNotNull(statusJira);
		
		
		List<Status> statusSdm = statusService.findByPlataforma(PlataformaEnum.SDM);
		assertNotNull(statusSdm);
		
		validaQuantidade(statusService.findAll(), statusJira.size() + statusSdm.size());
	}
	
	@Test
	void findByPlataformaAndTipoAtendimento() {
		List<Status> statusJiraPB = statusService.findByPlataformaAndTipoAtendimento(PlataformaEnum.JIRA, TipoAtendimentoEnum.PB);
		assertNotNull(statusJiraPB);
		
		
		List<Status> statusJiraInc = statusService.findByPlataformaAndTipoAtendimento(PlataformaEnum.JIRA, TipoAtendimentoEnum.INC);
		assertNotNull(statusJiraInc);
		
		
		List<Status> statusSdmPB = statusService.findByPlataformaAndTipoAtendimento(PlataformaEnum.SDM, TipoAtendimentoEnum.PB);
		assertNotNull(statusSdmPB);
		
		
		List<Status> statusSdmInc = statusService.findByPlataformaAndTipoAtendimento(PlataformaEnum.SDM, TipoAtendimentoEnum.INC);
		assertNotNull(statusSdmInc);
		
		validaQuantidade(statusService.findAll(), (statusSdmInc.size() + statusSdmPB.size() + statusJiraInc.size() + statusJiraPB.size()));
	}
	
	@Test
	void findById() {
		Status s = statusService.findById(1);
		assertNotNull(s);
		assertEquals(s.getNome(), "Aberto");
	}
	
	private void validaQuantidade(List<Status> s, Integer qtd) {
		if(!s.isEmpty() && s != null) {
			assertEquals(s.size(), qtd);
		}
	}
}
