package com.magna.gerenciadordechamados;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.magna.gerenciadordechamados.enums.TipoAtendimentoEnum;
import com.magna.gerenciadordechamados.model.Usuario;
import com.magna.gerenciadordechamados.service.StatusService;
import com.magna.gerenciadordechamados.service.UsuarioService;

@SpringBootTest
class GerenciadorDeChamadosApplicationTests {

	@Autowired
	UsuarioService usuarioService;
	
	@Autowired
	StatusService statusService;

	void contextLoads() {
	}

	@Test
	void buscar() {
		assertNotNull(usuarioService);
		usuarioService.getAllUsuarios().forEach(usuario -> System.out.println(usuario.getNome()));
	}
	
	
	void salvar() {
		Usuario usuario = new Usuario();
		usuario.setNome("Zaggia viado");
		usuario.setEmail("Zagia.viado@gmail.com");
		
		usuarioService.saveOrUpdate(usuario);	
	}
	
	
	void upadte() {
		Usuario usuario = usuarioService.getUsuario(3);
		
		System.out.println(usuario.getIdUsuario());
		System.out.println(usuario.getNome());
		System.out.println(usuario.getEmail());
		
		usuarioService.delete(usuario.getIdUsuario());
		
		System.out.println(usuarioService.count());
		
		assertEquals(1L, usuarioService.count());
		
	}
	@Test
	void buscarSatatus() {
		statusService.findByTipoAtendimento(TipoAtendimentoEnum.PB).forEach(status -> System.out.println(status.getNome()));
	}

}
